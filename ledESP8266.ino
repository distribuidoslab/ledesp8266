// TUTORIALES UTILIZADOS PARA EL PROYECTO:
// https://www.youtube.com/watch?v=noT_Mrfstdw (VIDEO)
// https://github.com/ProjectoOfficial/ESP32 (GITHUB)

#include <ESP8266WiFi.h>
 
const char* ssid = "ssid";
const char* password = "password";
  
int ledPin = 2; 
WiFiServer server(80);
 
void setup() {
  
  Serial.begin(115200);
  delay(10);
 
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
 
  // Conexión a la red WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectando a ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.println("WiFi conectado");
 
  server.begin();
  Serial.println("Servidor iniciado");
 
  Serial.print("Utilice esta URL: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");
}
 
void loop() {
  
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  Serial.println("Nuevo Cliente");
  
  while(!client.available()){
    delay(1);
  }
 
  String request = client.readStringUntil('\r');
  Serial.println(request);
  
  client.flush();

  int estado;

  if (request.indexOf("/") != -1)  {
    //estado = LOW;
    estado = HIGH;
    digitalWrite(ledPin, estado);
  }

  if (request.indexOf("/LED=OFF") != -1)  {
    //estado = LOW;
    estado = HIGH;
    digitalWrite(ledPin, estado);
  }
  
  if (request.indexOf("/LED=ON") != -1)  {
    //estado = HIGH;
    estado = LOW;
    digitalWrite(ledPin, estado);
  }
  // ACLARACION, EL LED ENCIENDE EN ESTADO "LOW" Y APAGA EN "HIGH" (DEBERIA SER AL REVES)
  // HTML
  client.println("<!DOCTYPE html><html>");
  client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
  client.println("<meta charset=\"UTF-8\">");
  client.println("<meta http-equiv=\"refresh\" content=\"5\" >");
  client.println("<link rel=\"icon\" href=\"data:,\">");
  client.println("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">");
  client.println("<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.6.3/css/all.css\" integrity=\"sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/\" crossorigin=\"anonymous\">");
  client.println("<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>");
  client.println("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>");
  client.println("<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>");

  client.println("<style>");
  client.println("html {font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
  client.println(".mybtn {padding: 16px 40px; font-size: 30px;} ");
  client.println(".par { font-size: 16px;}");
  client.println("p {text-align: center;}");
  client.println(".lm35 {text-align: center; border: none; margin: 2px; padding: 16px 40px; font-size: 30px;}");
  client.println("</style></head>");

  client.println("<body class=\"bg-light\">");
  client.println("<h1 style=\"text-align:center; \" class=\"display-4\">ESP8266 Servidor Web </h1> ");

  // ESTADO APAGADO
  if(estado == HIGH) {
    client.println("<p><a href =\"/LED=ON\" role=\"button\" class=\"btn btn-warning mybtn\" >Encender</a></p>");
  } else {
    client.println("<p><a href =\"/LED=OFF\" role=\"button\" class=\"btn btn-secondary mybtn\" >Apagar</a></p>");
  }

  client.println("<footer class=\"bg-dark text-center text-white\">");
  client.println("<div class=\"text-center p-3\" style=\"background-color: rgba(0, 0, 0, 0.2);\"> Grupo 4: Tatiana M. Krayeski - Jorge F. Rodriguez </div>");
  client.println("</footer>");
  client.println("</body></html>");
 
  delay(1);
  Serial.println("Cliente desconectado");
  Serial.println("");
}
